var express = require('express');
var router = express.Router();
var employee_dal = require('../model/employee_dal');
var stage_dal = require('../model/stage_dal');

// View All employee
router.get('/all', function(req, res) {
    employee_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('employee/employeeViewAll', { 'result':result });
        }
    });

});

router.get('/booked', function(req, res) {
    employee_dal.getEmployeesBooked(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('employee/employeeBookedArtist', { 'result':result });
        }
    });

});

// View the employee for the given id
router.get('/', function(req, res){
    if(req.query.employee_id == null) {
        res.send('employee_id is null');
    }
    else {
       employee_dal.getById(req.query.employee_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('employee/employeeViewById', {'result': result});
           }
        });
    }
});

// Return the add a a new employee form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    stage_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('employee/employeeAdd', {stage: result});
        }
    });
});

// Insert a new employee
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.fname == "") {
        res.send('Please provide an first name.');
    }
    else if(req.query.lname == "") {
        res.send('Please provide an email.');
    }
    else if(req.query.phone == "") {
        res.send('Please provide a phone number.');
    }
    else if(req.query.stage_id == null) {
        res.send('Please select a stage.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        employee_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                employee_dal.getAll(function(err, result){
                    if(err) {
                        res.send(err);
                    }
                    else {
                        res.render('employee/employeeViewAll', { 'result':result, was_successful:true });
                    }
                });
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.employee_id == null) {
        res.send('An employee id is required');
    }
    else {
        employee_dal.edit(req.query.employee_id, function(err, result){
            res.render('employee/employeeUpdate', {employee: result[0][0], stage: result[1]});
        });
    }
});



router.get('/update', function(req, res) {
    employee_dal.update(req.query, function(err, result){
        employee_dal.getById(req.query.employee_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('employee/employeeViewById', {'result': result, was_successful: true});
            }
        });
    });
});

// Delete an employee for the given company_id
router.get('/delete', function(req, res){
    if(req.query.employee_id == null) {
        res.send('employee_id is null');
    }
    else {
         employee_dal.delete(req.query.employee_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/employee/all');
             }
         });
    }
});

module.exports = router;
